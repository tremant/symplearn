# src/symplearn/architectures.py

from dataclasses import dataclass, asdict

import torch
from torch import nn
import pickle

def str_to_activation(name: str) -> nn.Module:
    """
    Converts a string representation of an activation function to the corresponding PyTorch activation function.

    Parameters
    ----------
    name : str
           The name of the activation function.

    Returns
    -------
    nn.Module
        The PyTorch activation function.

    Raises
    ------
    KeyError
        If the given name does not match any supported activation function.
    """
    key = name.lower()
    nonlinearities = {
        'elu:': nn.ELU,
        'relu': nn.ReLU,
        'selu': nn.SELU,
        'sigmoid': nn.Sigmoid,
        'silu': nn.SiLU,
        'softplus': nn.Softplus,
        'swish': nn.SiLU,
        'tanh': nn.Tanh,
    }

    if key not in nonlinearities.keys():
        raise KeyError(f"Could not match '{name}' to an activation function.")
    return nonlinearities[key]


@dataclass
class ConfigMLP:
    """
    Configuration class for Multi-Layer Perceptron (MLP) architecture.

    Attributes
    ----------
        input_dim : int
            The dimensionality of the input data.
        output_dim : int
            The dimensionality of the output data.
        inner_layers : list[int])
            A list of integers representing the number of units in each hidden layer.
        activation : str
            The activation function to be used in the hidden layers.
        final_bias : bool, default=True
            Whether to include a bias term in the final layer.
    """
    input_dim: int
    output_dim: int
    inner_layers: list[int]
    activation: str
    final_bias: bool = True


class MultiLayerPerceptron(nn.Module):
    """
    A multi-layer perceptron (MLP) neural network model.
    
    TODO: unit tests + can the save be a `to_dict` instead?

    Parameters
    ----------
    config : ConfigMLP
        Configuration object containing model parameters.
    **kwargs: dict, optional
        Additional keyword arguments for the activation function.

    Attributes
    ----------
    model : nn.Sequential
        The sequential model representing the MLP.

    """

    def __init__(self, config: ConfigMLP, **kwargs) -> None:
        super().__init__()
        self.config = config
        
        in_dims = [config.input_dim] + config.inner_layers
        out_dims = config.inner_layers
        activ = str_to_activation(config.activation)
        layers = []
        for in_dim, out_dim in zip(in_dims, out_dims):
            layers.append(nn.Linear(in_dim, out_dim))
            layers.append(activ(**kwargs))
        layers.append(nn.Linear(in_dims[-1], config.output_dim, bias=config.final_bias))
        self.model = nn.Sequential(*layers)

    def forward(self, inputs):
        """
        Forward pass of the MLP.

        Parameters
        ----------
        inputs : torch.Tensor
                 Input tensor.

        Returns
        -------
        torch.Tensor
            Output tensor.
        """
        return self.model(inputs)

    def saveto(self, fname):
        """
        Save the model to a file.

        Parameters
        ----------
        fname : str
            The name of the file to save the model to.

        """
        pickle.dump(
            {'config': self.config, 'params': self.model.state_dict()}, 
            open(fname, 'wb'))

    @classmethod
    def load_from(self, fname):
        """
        Load the model from a file.

        Parameters
        ----------
        fname : str
                The name of the file to load the model from.

        Returns
        ----------
        MultiLayerPerceptron
            The loaded model.

        """
        data = pickle.load(open(fname, 'rb'))
        model = MultiLayerPerceptron(data['config'])
        model.model.load_state_dict(data['params'])
        return model
