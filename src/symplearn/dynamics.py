from abc import ABC, abstractmethod

import numpy as np
from scipy.integrate import solve_ivp

import torch
from torch import autograd as ad


class AbstractDiffEq(ABC):
    """
    Abstract base class for defining a differential equation.

    Attributes
    ----------
    dim : int
        The dimension of the differential equation.

    Methods
    -------
    vector_field(z)
        Computes the vector field of the differential equation.
    np_vector_field(t, z)
        Computes the vector field of the differential equation using NumPy (`t` is a dummy argument).
    generate_trajectories(z0, tf, nt, rtol=1e-10, atol=1e-10, method='DOP853', **kwargs)
        Generates trajectories from initial conditions using `solve_ivp`.
    """
    def __init__(self, dim):
        self.dim = dim
    
    @abstractmethod
    def vector_field(self, z):
        pass
    
    def np_vector_field(self, t, z):
        return self.vector_field(torch.Tensor(z)).detach().cpu().numpy()

    def generate_trajectories(
            self,
            z0: torch.Tensor,
            tf: float,
            nt: int, *,
            rtol: float = 1e-10,
            atol: float = 1e-10,
            method: str = 'DOP853',
            **kwargs
        ) -> tuple[torch.Tensor, torch.Tensor]:
        """
        Generate trajectories from initial conditions `z0` up to time `tf` 
        with `nt` time-steps, wrapping `scipy.integrate.solve_ivp`.

        Parameters
        ----------
            z0 : torch.Tensor
                The initial conditions for the trajectories, 
                of shape `[num_traj, dim]`.
            tf : float
                The final time for the trajectories.
            nt : int
                The number of time-steps (`nt+1` points in total).
            rtol : float, default=1e-10
                The relative tolerance for the solver.
            atol : float, default=1e-10
                The absolute tolerance for the solver.
            method : str, default='DOP853'
                The integration method to use.
            **kwargs : dict, optional
                Additional keyword arguments to be passed to `solve_ivp`.

        Returns
        -------
            t_eval : torch.Tensor
                The time points at which the trajectories are evaluated.
            z : torch.Tensor
                The generated trajectories.
        """
        t_eval = np.linspace(0.0, tf, nt+1)
        u0 = z0.cpu().numpy().reshape(-1)
        vf = lambda t, z: self.np_vector_field(t, z.reshape(-1, self.dim)).reshape(-1)
        solver_args = kwargs | dict(t_eval=t_eval, rtol=rtol, atol=atol, method=method)
        sol = solve_ivp(vf, (0.0, tf), u0, **solver_args)
        y = sol.y.reshape(-1, self.dim, nt+1) # (n_traj, z_dim, nt+1)
        z = np.transpose(y, (0, 2, 1)) # (n_traj, nt+1, z_dim)
        t_eval, z = torch.tensor(t_eval), torch.tensor(z)
        return t_eval, z


class SplitSymplecticTwoForm(torch.Tensor):
    @staticmethod 
    def __new__(cls, dq, *args, **kwargs):
        ext_dq = torch.cat((dq, torch.zeros_like(dq)), -2)
        return torch.Tensor.__new__(cls, ext_dq.transpose(-1,-2) - ext_dq, *args, **kwargs)
    #
    def __init__(self, dq):
        super().__init__()
        self.dq = dq
        xdim = dq.shape[-1] // 2
        self.wxx = self[..., :xdim, :xdim]
        self.wxy = self[..., :xdim, xdim:]
        self.wyx = self[..., xdim:, xdim:]
    #
    def inv(self):
        byx = torch.linalg.inv(self.wxy)
        bxx = torch.zeros_like(self.wxx)
        bxy = -byx.transpose(-1, -2)
        byy = torch.matmul(byx, torch.matmul(self.wxx, -bxy))
        return torch.cat((torch.cat((bxx, bxy), -1), torch.cat((byx, byy), -1)), -2)
    #
    def solve(self, dh):
        dx_h, dy_h = torch.tensor_split(dh, 2, -1)
        dx = torch.linalg.solve(self.wyx, -dy_h)
        rhs_y = dx_h - torch.einsum('...ij, ...j -> ...i', self.wxx, dx)
        dy = torch.linalg.solve(self.wxy, rhs_y)
        return torch.cat((dx, dy), -1)


class CanonicalSymplecticTwoForm(SplitSymplecticTwoForm):
    def inv(self):
        return -self

    def solve(self, dh):
        dq_h, dp_h = torch.tensor_split(dh, 2, -1)
        return torch.cat((dp_h, -dq_h), -1)


class AbstractOneform:
    """
    Represents the symplectic two-form generated from a potential [..., 2d] -> [..., d].

    This class defines an abstract base class for representing a symplectic two-form generated from a potential.
    Subclasses of this class should implement the `__call__` method.

    Attributes
    ----------
        None

    Methods
    -------
    __call__(*args, **kwds)
        Abstract method that represents the action of the one-form on the input.
    with_jacobian(z)
        Computes the differential of the one-form using automatic differentiation,
        [..., 2d] -> ([..., d], [..., d, 2d]).
    twoform(z)
        Computes the differential of the one-form using automatic differentiation and 
        constructs the two-form with `SplitSymplecticTwoForm`.

    Usage
    -----
    To use this class, create a subclass that implements the required methods.
    """
    
    def __init__(self, *, sympformtype = SplitSymplecticTwoForm):
        self.sympformtype = sympformtype
    
    @abstractmethod
    def __call__(self, *args, **kwds):
        pass
    
    def with_jacobian(self, z):
        z = z.requires_grad_(True)
        q = self(z)
        dq = torch.cat([ad.grad(q[..., i].sum(), z, create_graph=True)[0].unsqueeze(-2)
                        for i in range(q.shape[-1])], -2)
        return q, dq

    def twoform(self, z):
        _, dq = self.with_jacobian(z)
        return self.sympformtype(dq)


class CanonicalOneform(AbstractOneform, ad.Function):
    """
    A class representing the canonical one-form u = (q,p) -> (p,0) = A.

    This class inherits from the `torch.autograd.Function` class and provides 
    methods for forward and jvp computations.

    Attributes
    ----------
        None

    Methods
    -------
    forward(ctx, input)
        Computes the forward pass of the canonical one-form.
    jvp(ctx, grad_input)
        Computes the jvp (Jacobian-vector product) of the canonical one-form.
    """
    
    def __init__(self):
        ad.Function.__init__(self)
        AbstractOneform.__init__(self, sympformtype=CanonicalSymplecticTwoForm)
    
    def __call__(self, u):
        return self.apply(u)
    
    @staticmethod
    def forward(ctx, u):
        # ctx.save_for_backward(input)
        q, p = torch.tensor_split(u, 2, -1)
        return p
    
    @staticmethod
    def vjp(ctx, dt_q):
        """Vector-jacobian product :math:`v^{\sf T}\cdot J` with :math:`J_{ij} = \frac{\partial f_i}{\partial x_j}`."""
        # input, = ctx.saved_tensors
        return dt_q
    
    def with_jacobian(self, u):
        qdim = u.shape[-1] // 2
        id_p = torch.diag_embed(torch.ones_like(u[..., :qdim]))
        dq = torch.cat((torch.zeros_like(id_p), id_p), -1)
        return u, dq


class HamiltonianDiffEq(AbstractDiffEq):
    """
    Canonical or non-canonical differential equation with a Hamiltonian 
    structure.
    
    .. warning:: 
        For now, the one-form is assumed to be a mapping `f: [..., dim] -> [..., dim//2]`,
        which would usually correspond to a full one-form `A = (f, 0)`.
        
    Attributes
    ----------
        dim : int
            The dimension of the space.
        hamiltonian : function
            A scalar function [..., dim] -> [..., 1].
        oneform : SymplecticForm, optional
            The one-form from which the symplectic two-form derives (default is None 
            in which case the structure is canonical).

    Parameters
    ----------
        dim : int
            Total dimension of the space -- should be even
        hamiltonian : function
            A scalar function [..., dim] -> [..., 1].
        oneform : SymplecticForm, optional
            The one-form from which the symplectic two-form derives (default is None 
            in which case the structure is canonical).
            
    Methods
    -------
        lagrangian(z, dx)
            Computes the Lagrangian of the system.
        assemble_two_form(q, z)
            Assembles the two-form from the one-form and the Hamiltonian.
        invert_two_form(w)
            Inverts the two-form, assuming it is a block matrix [dx_q.T - dx_q dy_q.T; -dy_q 0].
        vector_field(z)
            Computes the vector field of the differential equation.
    """

    def __init__(self, dim, hamiltonian, *, oneform = None):
        AbstractDiffEq.__init__(self, dim)
        if oneform is None:
            self.oneform = CanonicalOneform.apply
        else:
            self.oneform = oneform
        self.hamiltonian = hamiltonian

    def lagrangian(self, x, y, dx):
        z = torch.cat((x, y), -1)
        q, h = self.oneform(z), self.hamiltonian(z)
        return torch.sum(q*dx, -1, True) - h
    
    def vector_field(self, z):
        h = self.hamiltonian(z)
        dh, = ad.grad(h.sum(), z, create_graph=True)
        w = self.oneform.twoform(z)
        return w.solve(dh)
