# SympLearn

[![pipeline status](https://gitlab.math.unistra.fr/tremant/symplearn/badges/main/pipeline.svg)](https://gitlab.math.unistra.fr/tremant/symplearn/-/commits/main)
[![coverage report](https://gitlab.math.unistra.fr/tremant/symplearn/badges/main/coverage.svg)](https://tremant.pages.math.unistra.fr/symplearn/coverage)
[![Latest Release](https://gitlab.math.unistra.fr/tremant/symplearn/-/badges/release.svg)](https://gitlab.math.unistra.fr/tremant/symplearn/-/releases)
[![Doc](https://img.shields.io/badge/doc-sphinx-blue)](https://tremant.pages.math.unistra.fr/symplearn)

<!--  -->

Geometric machine learning  and long-time simulations in Python

## To-do

- [ ] dynamics
  - [ ] DifferentialEquation
  - [ ] CanonicalHamiltonianDynamics
  - [ ] HamiltonianDynamics (degenerate Lagrangian)
- [ ] numerical schemes
  - [ ] `solve_ivp`
  - [ ] midpoint
  - [ ] DVI
- [ ] architectures
  - [ ] config files
  - [ ] MLP
  - [ ] save & load from config
- [ ] data generation
  - [ ] sample init cond
  - [ ] from VF
  - [ ] small trajectories
- [ ] train
  - [ ] loss routines (discrete scheme, continuous)
  - [ ] save params & trace
  - [ ] training parameters
- [ ] test cases
  - [ ] Lotka-Volterra
  - [ ] mag. field lines
  - [ ] guiding center

## Features

- todo
- more todo

See [Documentation](https://tremant.pages.math.unistra.fr/symplearn) for details.
