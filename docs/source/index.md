# SympLearn

[![pipeline status](https://gitlab.math.unistra.fr/tremant/symplearn/badges/main/pipeline.svg)](https://gitlab.math.unistra.fr/tremant/symplearn/-/commits/main)
[![coverage report](https://gitlab.math.unistra.fr/tremant/symplearn/badges/main/coverage.svg)](https://tremant.pages.math.unistra.fr/symplearn/coverage)
[![Latest Release](https://gitlab.math.unistra.fr/tremant/symplearn/-/badges/release.svg)](https://gitlab.math.unistra.fr/tremant/symplearn/-/releases)

## Description

Geometric machine learning  and long-time simulations in Python

## Features

- everything has
- to be done

## Modules

```{eval-rst}
.. autosummary::
   :toctree: _autosummary
   :caption: Modules
   :recursive:

   symplearn.symplearn
```

## Installation

### Install python package

#### Using pip

Your ssh key must be present on <https://gitlab.math.unistra.fr/-/profile/keys>.

```bash
pip install git+ssh://git@gitlab.math.unistra.fr/tremant/symplearn.git
```

#### Using pip in a virtual environment

```bash
git clone git@gitlab.math.unistra.fr:tremant/symplearn.git
cd symplearn
python3 -m virtualenv .venv  # create a virtual environment
source .venv/bin/activate  # activate the virtual environment
pip install -e .  # install the package in editable mode
```

Note: in editable mode (`-e` option), the package is installed in a way that it is still possible to edit the source code and have the changes take effect immediately.

### Run the unitary tests

#### Install the development dependencies

```bash
pip install -e .[test]
```

#### Run the tests

Run the tests from the projet root directory using the `-s`:

```bash
pytest -sv
```

See [.gitlab-ci.yml](https://gitlab.math.unistra.fr/tremant/symplearn/blob/main/.gitlab-ci.yml) for more details.

### Build the documentation

#### Install the documentation dependencies

```bash
pip install -e .[doc]
```

#### Build and serve the documentation locally

```bash
sphinx-autobuild docs/source/ docs/_build/html
```

Go to <http://localhost:8000> and see the changes in `docs/source/` directory take effect immediately.

## Indices and tables

* {ref}`genindex`
* {ref}`modindex`
